package com.example.fingerprinttest;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.fingerprinttest.fingerprint.FingerPrintAuth;
import com.example.fingerprinttest.fingerprint.FingerPrintStatus;
import com.example.fingerprinttest.fingerprint.FingerprintAuthListener;

/**
 * Created by sayeedm on 3/5/17.
 */

public class MainActivity  extends AppCompatActivity implements FingerprintAuthListener{

    private TextView statusText;
    private Button button;

    @Override
    public void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        setContentView(R.layout.main);

        statusText = (TextView)findViewById(R.id.status);
        button = (Button)findViewById(R.id.auth);

        final FingerPrintAuth auth = FingerPrintAuth.getInstance();
        auth.init(this, true);
        auth.setFingerprintAuthListener(this);

        FingerPrintStatus status = auth.checkFingerPrintModuleStatus();


        if (status == FingerPrintStatus.OK) {
            button.setEnabled(true);
            status = auth.createSignature();
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    auth.authenticate();
                }
            });
        }else{
            statusText.setTextColor(Color.RED);
            statusText.setText(status.toString());
            button.setEnabled(false);
            button.setOnClickListener(null);
        }



    }

    @Override
    public void onAuthenticationSuccess() {
        statusText.setTextColor(Color.GREEN);
        statusText.setText("Success !!!");
    }

    @Override
    public void onAuthenticationFailure() {
        statusText.setTextColor(Color.RED);
        statusText.setText("Failed !!!");
    }
}
