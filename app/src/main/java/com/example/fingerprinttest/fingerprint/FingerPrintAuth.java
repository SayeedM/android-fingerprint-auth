package com.example.fingerprinttest.fingerprint;

import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;

/**
 * Created by sayeedm on 3/5/17.
 */

public class FingerPrintAuth implements FingerprintAuthListener{

    private Context context;
    private FingerPrintHandler handler;
    private static FingerPrintAuth instance = null;
    private boolean useAsymmetricKey = false;

    public static FingerPrintAuth getInstance(){
        if (instance == null)
            instance = new FingerPrintAuth();
        return instance;
    }

    public void init(Context context, boolean useAsymmetricKey){
        this.context = context;
        handler = new FingerPrintHandler(this);
        this.useAsymmetricKey = useAsymmetricKey;
    }

    private FingerPrintAuth(){

    }

    private static final String KEY_NAME = "MY_KEY";

    private KeyguardManager keyguardManager;
    private FingerprintManager fingerprintManager;

    private FingerprintManager.CryptoObject cryptoObject;

    private FingerPrintKeyGenerator generator;

    public FingerPrintStatus checkFingerPrintModuleStatus(){

        if (context == null)
            return FingerPrintStatus.MODULE_NOT_INIT;

        if (Build.VERSION.SDK_INT < 23)
            return FingerPrintStatus.ANDROID_VERSION_NOT_SUPPORTED;


        // Keyguard Manager
        keyguardManager = (KeyguardManager) context.getSystemService(KEYGUARD_SERVICE);

        // Fingerprint Manager
        fingerprintManager = (FingerprintManager) context.getSystemService(FINGERPRINT_SERVICE);

        try {
            if (!fingerprintManager.isHardwareDetected())
                return FingerPrintStatus.HARDWARE_NOT_SUPPORTED;

            if (!fingerprintManager.hasEnrolledFingerprints())
                return FingerPrintStatus.NO_FINGER_ENROLLED;


            if (!keyguardManager.isKeyguardSecure())
                return FingerPrintStatus.NO_SECURITY_CONFIGURED;
        }catch(SecurityException se) {
            se.printStackTrace();
            return FingerPrintStatus.SECURITY_ERROR;
        }

        return FingerPrintStatus.OK;

    }




    @TargetApi(23)
    public FingerPrintStatus createSignature(){

        if (!useAsymmetricKey)
            generator = new SymmetricFingerPrintKeyGenerator(KEY_NAME);
        else
            generator = new AsymmetricFingerPrintKeyGenerator(KEY_NAME);

        generator.generateKey();
        this.cryptoObject = generator.createCryptoObject();

        if (cryptoObject != null)
            return FingerPrintStatus.OK;

        return FingerPrintStatus.KEY_GENERATION_ERROR;

    }

    public void authenticate(){
        handler.doAuth(fingerprintManager, cryptoObject);
    }


    private FingerprintAuthListener listener;

    public void setFingerprintAuthListener(FingerprintAuthListener listener){
        this.listener = listener;

    }

    @Override
    public void onAuthenticationSuccess() {
        if (listener != null)
            listener.onAuthenticationSuccess();
    }

    @Override
    public void onAuthenticationFailure() {
        if (listener != null)
            listener.onAuthenticationFailure();
    }
}
