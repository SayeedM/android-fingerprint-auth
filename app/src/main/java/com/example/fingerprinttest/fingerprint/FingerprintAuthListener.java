package com.example.fingerprinttest.fingerprint;

/**
 * Created by sayeedm on 3/5/17.
 */

public interface FingerprintAuthListener {
    void onAuthenticationSuccess();
    void onAuthenticationFailure();
}
