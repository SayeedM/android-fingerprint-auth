package com.example.fingerprinttest.fingerprint;

import android.hardware.fingerprint.FingerprintManager;

/**
 * Created by sayeedm on 3/5/17.
 */
public interface FingerPrintKeyGenerator {
    FingerPrintStatus generateKey();
    FingerprintManager.CryptoObject createCryptoObject();
}
