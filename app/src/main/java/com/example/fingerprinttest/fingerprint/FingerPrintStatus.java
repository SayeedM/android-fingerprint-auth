package com.example.fingerprinttest.fingerprint;

/**
 * Created by sayeedm on 3/5/17.
 */

public enum FingerPrintStatus {
    OK,
    HARDWARE_NOT_SUPPORTED,
    ANDROID_VERSION_NOT_SUPPORTED,
    NO_FINGER_ENROLLED,
    NO_SECURITY_CONFIGURED,
    SECURITY_ERROR,
    MODULE_NOT_INIT,
    KEY_GENERATION_ERROR,
    CIPHER_GENERATION_ERROR
}
