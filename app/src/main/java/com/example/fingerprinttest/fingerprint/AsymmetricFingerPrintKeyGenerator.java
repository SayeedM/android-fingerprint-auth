package com.example.fingerprinttest.fingerprint;

import android.annotation.TargetApi;
import android.hardware.fingerprint.FingerprintManager;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.ECGenParameterSpec;

/**
 * Created by sayeedm on 3/5/17.
 */

public class AsymmetricFingerPrintKeyGenerator implements FingerPrintKeyGenerator {

    private String KEY_NAME;

    public AsymmetricFingerPrintKeyGenerator(String KEY_NAME){
        this.KEY_NAME = KEY_NAME;
    }

    private KeyPairGenerator keyPairGenerator;

    @TargetApi(23)
    @Override
    public FingerPrintStatus generateKey() {
        try {
            keyPairGenerator = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_EC, "AndroidKeyStore");
            keyPairGenerator.initialize(
                    new KeyGenParameterSpec.Builder(KEY_NAME,
                            KeyProperties.PURPOSE_SIGN)
                            .setDigests(KeyProperties.DIGEST_SHA256)
                            .setAlgorithmParameterSpec(new ECGenParameterSpec("secp256r1"))
                            .setUserAuthenticationRequired(true)
                            .build());
            keyPairGenerator.generateKeyPair();
            return FingerPrintStatus.OK;
        }catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException | NoSuchProviderException ex){
            ex.printStackTrace();
            return FingerPrintStatus.KEY_GENERATION_ERROR;
        }
    }

    @TargetApi(23)
    @Override
    public FingerprintManager.CryptoObject createCryptoObject() {
        try {
            Signature signature = Signature.getInstance("SHA256withECDSA");
            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
            PrivateKey key = (PrivateKey) keyStore.getKey(KEY_NAME, null);
            signature.initSign(key);
            FingerprintManager.CryptoObject cryptObject = new FingerprintManager.CryptoObject(signature);
            return cryptObject;
        }catch (NoSuchAlgorithmException | KeyStoreException | UnrecoverableKeyException | CertificateException |
                IOException  | InvalidKeyException ex){
            ex.printStackTrace();
            return null;
        }
    }

    public PublicKey getPublicKey(){
        try {
            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
            PublicKey publicKey =
                    keyStore.getCertificate(KEY_NAME).getPublicKey();
            return publicKey;

//            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
//            keyStore.load(null);
//            PrivateKey key = (PrivateKey) keyStore.getKey(KEY_NAME, null);
        }catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException ex){
            ex.printStackTrace();
            return null;
        }
    }
}
