package com.example.fingerprinttest.fingerprint;


import android.annotation.TargetApi;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;


@TargetApi(23)
public class FingerPrintHandler extends FingerprintManager.AuthenticationCallback {


    private FingerprintAuthListener listener;

    public FingerPrintHandler(FingerprintAuthListener listener) {
        this.listener = listener;
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        super.onAuthenticationError(errorCode, errString);
        System.out.println("auth failed");
        if (listener != null)
            listener.onAuthenticationFailure();
    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        super.onAuthenticationHelp(helpCode, helpString);
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        super.onAuthenticationSucceeded(result);
        System.out.println("auth ok");

        if (result.getCryptoObject().getCipher() != null)
            System.out.println("Success Cipher : " + result.getCryptoObject().getCipher().toString());



        if (result.getCryptoObject().getMac() != null)
            System.out.println("Success Mac : " + result.getCryptoObject().getMac().toString() );

        if (result.getCryptoObject().getSignature() != null)
            System.out.println("Success Signature : " + result.getCryptoObject().getSignature().toString());

        if (listener != null)
            listener.onAuthenticationSuccess();

    }

    @Override
    public void onAuthenticationFailed() {
        super.onAuthenticationFailed();
        if (listener != null)
            listener.onAuthenticationFailure();
    }

    @TargetApi(23)
    public void doAuth(FingerprintManager manager, FingerprintManager.CryptoObject obj) {
        CancellationSignal signal = new CancellationSignal();
        try {
            manager.authenticate(obj, signal, 0, this, null);
        }catch(SecurityException sce) {
            sce.printStackTrace();
        }

    }

}