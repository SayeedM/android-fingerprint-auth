package com.example.fingerprinttest.fingerprint;

import android.annotation.TargetApi;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/**
 * Created by sayeedm on 3/5/17.
 */

public class SymmetricFingerPrintKeyGenerator implements FingerPrintKeyGenerator {

    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private String KEY_NAME;


    public SymmetricFingerPrintKeyGenerator(String KEY_NAME){
        this.KEY_NAME = KEY_NAME;
    }

    @Override
    public FingerPrintStatus generateKey() {
        if (Build.VERSION.SDK_INT < 23)
            return FingerPrintStatus.ANDROID_VERSION_NOT_SUPPORTED;

        try {

            // Get the reference to the key store
            keyStore = KeyStore.getInstance("AndroidKeyStore");

            // Key generator to generate the key
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            keyStore.load(null);

            keyGenerator.init(new
                            KeyGenParameterSpec.Builder(
                            KEY_NAME,
                            KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT
                    )
                            .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                            .setUserAuthenticationRequired(true)
                            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                            .build()
            );

            keyGenerator.generateKey();
        }catch(KeyStoreException | NoSuchAlgorithmException | NoSuchProviderException |
                    InvalidAlgorithmParameterException | CertificateException | IOException exc) {

                exc.printStackTrace();
                return FingerPrintStatus.KEY_GENERATION_ERROR;


        }
        return FingerPrintStatus.OK;
    }

    @TargetApi(23)
    @Override
    public FingerprintManager.CryptoObject createCryptoObject() {
        FingerPrintStatus status = generateKey();
        if (status == FingerPrintStatus.OK) {
            try {
                Cipher cipher = generateCipher();

                if (cipher == null)
                    return null;

                return new FingerprintManager.CryptoObject(cipher);
            }catch (Exception ex){
                ex.printStackTrace();
                return null;
            }
        }else{
            return null;
        }
    }


    private Cipher generateCipher() {
        try {
            Cipher cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/" +
                            KeyProperties.BLOCK_MODE_CBC + "/" +
                            KeyProperties.ENCRYPTION_PADDING_PKCS7);

            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME, null);
            cipher.init(Cipher.ENCRYPT_MODE, key);

            return cipher;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | UnrecoverableKeyException | KeyStoreException exc) {
            exc.printStackTrace();
            return null;
        }
    }
}
